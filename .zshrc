
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=#bd93f9'
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
ZSH_HIGHLIGHT_MAXLENGTH=512
export HISTFILESIZE=100000000
export HISTSIZE=100000000
export HISTFILE=~/.zsh_history
export SAVEHIST=5000

setopt HIST_FIND_NO_DUPS
# following should be turned off, if sharing history via setopt SHARE_HISTORY
setopt INC_APPEND_HISTORY

#FZF Presets

export FZF_DEFAULT_OPTS='
  --color fg:255,hl:84,fg+:255,hl+:215
  --color info:141,prompt:84,spinner:212,pointer:212,marker:212
  --border=rounded
'
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'
export FZFZ_RECENT_DIRS_TOOL='fasd'
export FZF_COMPLETION_TRIGGER='~~'
export FZF_COMPLETION_OPTS='--border --info=inline'

_fzf_compgen_path() {
  fd --hidden --follow --exclude ".git" . "$1"
}

_fzf_compgen_dir() {
  fd --type d --hidden --follow --exclude ".git" . "$1"
}

#export DISPLAY=':0'
export XDG_CONFIG_HOME="$HOME/.config"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="/usr/local/texlive/2021/bin/x86_64-linux:$PATH"
export PATH="$HOME/.npm/bin:$PATH"
export PATH="$HOME/node_modules/.bin:$PATH"
export EDITOR="nvim"
export BROWSER=qutebrowser


#alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
export DOTBARE_DIR="$HOME/.cfg"
export DOTBARE_TREE="$HOME"
alias config=dotbare

#task warrior configuration
export TASKRC="$HOME/.local/share/task/.taskrc"
export TIMEWARRIORDB="$HOME/.local/share/task/timew"
alias taskopen="taskopen -c $HOME/.local/share/task/taskopen/.taskopenrc"

alias fm="vifmrun"
alias cl="clear"
alias sudo="doas"
alias ls="exa --icons"
alias pick="pastel pick"
alias yout="ytfzf -t"
alias web="w3m google.com"
alias vim="/usr/bin/nvim"
alias cat="bat"
alias fzf="fzf --preview 'bat --color=always --style=numbers --line-range=:500 {}'"
alias m='f -e mpv' # quick opening files with mpv 
alias v='f -e nvim' # quick opening files with vim
alias mpv='devour mpv'
alias tw='taskwarrior-tui'
alias ht='btm'
alias grubupdate='sudo grub-mkconfig -o /boot/grub/grub.cfg'
alias bye="sudo loginctl poweroff"
alias reb="sudo loginctl reboot"
#alias cpu="cpufetch"

# XBPS
alias update="sudo xbps-install -Su"
alias install="sudo xbps-install -S"
alias query="xbps-query -Rs"
alias remove="sudo xbps-remove -ROo"

#VPN
alias killvpn="sudo windscribe stop && sudo resolvconf -u && notify-send Windscribe OFF"
alias openvpnfr="sudo windscribe start && windscribe connect FR && notify-send Winscribe OK"
alias openvpnus="sudo windscribe start && windscribe connect US && notify-send Winscribe OK"

rsfetch -Nl -L ~/.logo

# autologin on tty1
 if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
 exec startx
 fi

#test
#
. ~/.config/zsh/fzf-z/fzf-z.plugin.zsh
. ~/.config/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh 
. ~/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
. ~/.config/zsh/zsh-vim-mode/zsh-vim-mode.plugin.zsh
. ~/.config/zsh/dotbare/dotbare.plugin.zsh
. /usr/share/doc/fzf/completion.zsh
. /usr/share/doc/fzf/key-bindings.zsh
. ~/.config/zsh/themes/dracula.zsh-theme

# Display st active directory 
PROMPT_COMMAND='printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/"~"}"'

eval "$(fasd --init auto)"
eval "$(starship init zsh)"

