import dracula.draw


# Load existing settings made via :set
config.load_autoconfig()

dracula.draw.blood(c, {
    'spacing': {
        'vertical': 6,
        'horizontal': 8
    }
})

#config.set("url.start_pages", "~/.config/qutebrowser/html/homepage.html")
#c.qt.flag = 'ignore-gpu-blacklist'
#c.qt.flag =  'enable-gpu-rasterization'
#c.qt.flag = 'enable-native-gpu-memory-buffers'



config.set("fonts.default_family", "12pt FiraCode Nerd Font Mono")
config.set("fonts.hints", "12pt FiraCode Nerd Font Mono")

config.set("fonts.hints", "12pt FiraCode Nerd Font Mono")
config.set("fonts.statusbar", "12pt FiraCode Nerd Font Mono")
config.set("fonts.tabs.selected", "12pt FiraCode Nerd Font Mono")
config.set("fonts.tabs.unselected", "12pt FiraCode Nerd Font Mono")
config.set("fonts.debug_console", "12pt FiraCode Nerd Font Mono")
config.set("fonts.messages.error", "12pt FiraCode Nerd Font Mono")
config.set("fonts.messages.info", "12pt FiraCode Nerd Font Mono")
config.set("fonts.messages.warning", "12pt FiraCode Nerd Font Mono")
config.set("fonts.prompts", "12pt FiraCode Nerd Font Mono")
config.set("fonts.downloads", "12pt FiraCode Nerd Font Mono")
config.set("colors.webpage.darkmode.enabled", True)
config.set("hints.uppercase", True)
config.set("tabs.show", "multiple")
config.set("tabs.tabs_are_windows", True)
config.set("statusbar.show", "in-mode")
config.set("hints.mode", "letter")
config.set("hints.radius", 0)
config.set("content.notifications.presenter", "libnotify")
config.set("content.geolocation", False)




c.tabs.padding = {'top': 5, 'bottom': 3, 'left': 10, 'right': 10}
config.set("fonts.completion.entry", "12pt FiraCode Nerd Font Mono")
config.set("zoom.default", "125%")
config.set("downloads.location.directory", "~/Downloads")
config.set("downloads.location.prompt", True)
config.set("content.user_stylesheets", "~/.config/qutebrowser/darculized-all-sites.css")


c.statusbar.padding = {'top': 4, 'bottom': 2, 'left': 10, 'right': 10}
c.statusbar.position = 'top'
c.tabs.favicons.scale = 1.0

c.input.spatial_navigation = False
c.prompt.filebrowser = True
c.prompt.radius = 10
c.scrolling.smooth = True


#KEYBINGS

config.bind(",m", "hint links spawn ~/.local/bin/umpv.py {hint-url}")

config.bind(",tv", "hint links spawn mpv --force-window=immediate --ytdl-format='bestvideo[ext=mp4][width<=426][height<=240]+bestaudio[ext=m4a]' --ytdl-raw-options=write-auto-sub=,sub-lang=en, {hint-url}")

config.bind(",e", "hint links spawn mpv --force-window=immediate --ytdl-format='bestvideo[ext=mp4][width<=1280][height<=720]+bestaudio[ext=m4a]' --ytdl-raw-options=write-auto-sub=,sub-lang=en, {hint-url}")

config.bind(",p", "hint links spawn mpv --force-window=immediate --ytdl-format='bestvideo[ext=mp4][width<=1280][height<=720]+bestaudio[ext=m4a]' --ytdl-raw-options=write-auto-sub=,sub-lang=pl, {hint-url}")

config.bind(",n", "hint links spawn mpv --force-window=immediate --ytdl-format='bestvideo[ext=mp4][width<=1280][height<=720]+bestaudio[ext=m4a]' --ytdl-raw-options=write-auto-sub=,sub-lang=no, {hint-url}")

config.bind(",x", "hint links userscript youtube_mp3_downloader.sh")
config.bind(",z", "hint links userscript youtube_downloader.sh")
config.bind(",c", "hint links userscript qute-copy-urls")
config.bind("<ctrl+j>", "completion-item-focus --history next", mode="command")
config.bind("<ctrl+k>", "completion-item-focus --history prev", mode="command")

#config.source('palenight-qutebrowser.py')
