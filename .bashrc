# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

#export DISPLAY=':0'
export XDG_CONFIG_HOME="$HOME/.config"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="/usr/local/texlive/2021/bin/x86_64-linux:$PATH"

export EDITOR="nvim"
export BROWSER=qutebrowser
alias grubupdate='sudo grub-mkconfig -o /boot/grub/grub.cfg'

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias fm="vifmrun"
alias cl="clear"
alias sudo="doas"
alias ls="exa --icons"
alias pick="pastel pick"
alias tv="ytfzf -t"
alias web="w3m google.com"
alias vim="/usr/bin/nvim"
alias cat="bat"
alias fzf="fzf --preview 'bat --color=always --style=numbers --line-range=:500 {}'"


alias bye="sudo loginctl poweroff"
alias reb="sudo loginctl reboot"

# XBPS
alias update="sudo xbps-install -Su"
alias install="sudo xbps-install -S"
alias query="xbps-query -Rs"
alias remove="sudo xbps-remove -ROo"

#VPN
alias killvpn="sudo windscribe stop && sudo resolvconf -u && notify-send Windscribe OFF"
alias openvpn="sudo windscribe start && windscribe connect FR && notify-send Winscribe OK"

rsfetch -lPdkrstNUu -p xbps -L ~/.logo

# autologin on tty1
# [[ $(fgconsole 2>/dev/null) == 1 ]]
# if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
#exec startx -- vt1 &> /dev/null
# fi


# Display st active directory 
PROMPT_COMMAND='printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/"~"}"'

eval "$(fasd --init auto)"

eval "$(starship init bash)"



