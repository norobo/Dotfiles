#!/bin/bash

(youtube-dl --write-auto-sub --sub-lang no,pl,fr,it --output "$HOME/Videos/00_Downloads/%(title)s.%(ext)s" $QUTE_URL && 
notify-send -i ~/Pictures/03_Icons/BagOfThings.png "YouTube download completed!" "$QUTE_TITLE") || exit 0
