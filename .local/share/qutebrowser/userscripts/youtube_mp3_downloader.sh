#!/bin/bash

(youtube-dl --output "$HOME/Music/00_Downloads/%(title)s.%(ext)s" -x --audio-format mp3 --audio-quality 0 $QUTE_URL && 
    notify-send -i ~/Pictures/03_Icons/Music.png "YouTube mp3 download completed!" "$QUTE_TITLE") || exit 0
